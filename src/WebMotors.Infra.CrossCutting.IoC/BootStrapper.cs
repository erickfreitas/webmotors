﻿using SimpleInjector;
using WebMotors.Application.AppServices;
using WebMotors.Application.Interfaces;
using WebMotors.Domain.Interfaces.Repositories;
using WebMotors.Domain.Interfaces.Services;
using WebMotors.Domain.Services;
using WebMotors.Infra.Data.Repositories;
using WebMotors.Infra.WebMotorsApi.Interfaces;
using WebMotors.Infra.WebMotorsApi.Services;

namespace WebMotors.Infra.CrossCutting.IoC
{
    public static class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            // Lifestyle.Transient => Uma instancia para cada solicitacao;
            // Lifestyle.Singleton => Uma instancia unica para a classe
            // Lifestyle.Scoped => Uma instancia unica para o request
            container.Register(typeof(IRepositoryBase<>), typeof(RepositoryBase<>), Lifestyle.Scoped);
            container.Register<IAnuncioRepository, AnuncioRepository>(Lifestyle.Scoped);
            container.Register<IWebMotorsApiService, WebMotorsApiService>(Lifestyle.Scoped);

            container.Register(typeof(IServiceBase<>), typeof(ServiceBase<>), Lifestyle.Scoped);
            container.Register<IAnuncioService, AnuncioService>(Lifestyle.Scoped);

            container.Register<IAnuncioAppService, AnuncioAppService>(Lifestyle.Scoped);
        }
    }
}
