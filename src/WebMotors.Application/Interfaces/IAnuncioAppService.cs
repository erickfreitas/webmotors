﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotors.Application.ViewModels;

namespace WebMotors.Application.Interfaces
{
    public interface IAnuncioAppService
    {
        AnuncioViewModel Add(AnuncioViewModel anuncioViewModel);
        AnuncioViewModel GetById(int id);
        IEnumerable<AnuncioViewModel> GetAll();
        AnuncioViewModel Update(AnuncioViewModel anuncioViewModel);
        void Remove(AnuncioViewModel anuncioViewModel);
        Task<IEnumerable<MakeViewModel>> GetMakes();
        Task<IEnumerable<ModelViewModel>> GetModelsByMake(int makeId);
        Task<IEnumerable<VersionViewModel>> GetVersionsByModel(int modelId);
        Task<IEnumerable<VehicleViewModel>> GetVehiclesByModel(int modelId);
        void Dispose();
    }
}
