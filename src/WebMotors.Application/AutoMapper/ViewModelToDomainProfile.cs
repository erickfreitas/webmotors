﻿using AutoMapper;
using WebMotors.Application.ViewModels;
using WebMotors.Domain.Entitities;

namespace WebMotors.Application.AutoMapper
{
    public class ViewModelToDomainProfile : Profile
    {
        public ViewModelToDomainProfile()
        {
            CreateMap<AnuncioViewModel, Anuncio>();
        }
    }
}
