﻿using AutoMapper;
using WebMotors.Application.ViewModels;
using WebMotors.Domain.Entitities;
using WebMotors.Infra.WebMotorsApi.Entities;

namespace WebMotors.Application.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            CreateMap<Anuncio, AnuncioViewModel>();
            CreateMap<Make, MakeViewModel>();
            CreateMap<Model, ModelViewModel>();
            CreateMap<Version, VersionViewModel>();
            CreateMap<Vehicle, VehicleViewModel>();
        }
    }
}
    