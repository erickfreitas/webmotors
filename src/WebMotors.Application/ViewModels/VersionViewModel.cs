﻿namespace WebMotors.Application.ViewModels
{
    public class VersionViewModel
    {
        public int ModelId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
