﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebMotors.Application.ViewModels
{
    public class AnuncioViewModel
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Marca")]
        [Required(ErrorMessage = "Informe a Marca do Veículo")]
        [MaxLength(45, ErrorMessage = "São permitidos no máximo 45 carácteres")]
        public string Marca { get; set; }

        [DisplayName("Modelo")]
        [Required(ErrorMessage = "Informe o Modelo do Veículo")]
        [MaxLength(45, ErrorMessage = "São permitidos no máximo 45 carácteres")]
        public string Modelo { get; set; }

        [DisplayName("Versão")]
        [Required(ErrorMessage = "Informe a Versão do Veículo")]
        [MaxLength(45, ErrorMessage = "São permitidos no máximo 45 carácteres")]
        public string Versao { get; set; }

        [DisplayName("Ano")]
        [Required(ErrorMessage = "Informe o Ano do Veículo")]
        public int Ano { get; set; }

        [DisplayName("Quilometragem")]
        [Required(ErrorMessage = "Informe a Quilometragem do Veículo")]
        public int Quilometragem { get; set; }

        [DisplayName("Observações")]
        [Required(ErrorMessage = "Informe as Observações do Veículo")]
        [MaxLength(255, ErrorMessage = "São permitidos no máximo 255 carácteres")]
        [MinLength(10, ErrorMessage = "São permitidos no mínimo 10 carácteres")]
        public string Observacao { get; set; }
    }
}
