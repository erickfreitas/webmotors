﻿namespace WebMotors.Application.ViewModels
{
    public class ModelViewModel
    {
        public int MakeId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
