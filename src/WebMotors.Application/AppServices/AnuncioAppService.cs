﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotors.Application.Interfaces;
using WebMotors.Application.ViewModels;
using WebMotors.Domain.Entitities;
using WebMotors.Domain.Interfaces.Services;
using WebMotors.Infra.WebMotorsApi.Entities;

namespace WebMotors.Application.AppServices
{
    public class AnuncioAppService : IAnuncioAppService
    {
        private readonly IAnuncioService _anuncioService;

        public AnuncioAppService(IAnuncioService anuncioService)
        {
            _anuncioService = anuncioService;
        }

        public AnuncioViewModel Add(AnuncioViewModel anuncioViewModel)
        {
            return Mapper.Map<Anuncio, AnuncioViewModel>(_anuncioService.Add(Mapper.Map<AnuncioViewModel, Anuncio>(anuncioViewModel)));
        }
        

        public IEnumerable<AnuncioViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Anuncio>, IEnumerable<AnuncioViewModel>>(_anuncioService.GetAll());
        }

        public AnuncioViewModel GetById(int id)
        {
            return Mapper.Map<Anuncio, AnuncioViewModel>(_anuncioService.GetById(id));
        }

        public async Task<IEnumerable<MakeViewModel>> GetMakes()
        {
            return Mapper.Map<IEnumerable<Make>, IEnumerable<MakeViewModel>>(await _anuncioService.GetMakes());
        }

        public async Task<IEnumerable<ModelViewModel>> GetModelsByMake(int makeId)
        {
            return Mapper.Map<IEnumerable<Model>, IEnumerable<ModelViewModel>>(await _anuncioService.GetModelsByMake(makeId));
        }

        public async Task<IEnumerable<VehicleViewModel>> GetVehiclesByModel(int modelId)
        {
            return Mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleViewModel>>(await _anuncioService.GetVehiclesByModel(modelId));
        }

        public async Task<IEnumerable<VersionViewModel>> GetVersionsByModel(int modelId)
        {
            return Mapper.Map<IEnumerable<Infra.WebMotorsApi.Entities.Version>, IEnumerable<VersionViewModel>>(await _anuncioService.GetVersionsByModel(modelId));
        }

        public void Remove(AnuncioViewModel anuncioViewModel)
        {
            var anuncio = _anuncioService.GetById(anuncioViewModel.Id);
            _anuncioService.Remove(anuncio);
        }

        public AnuncioViewModel Update(AnuncioViewModel anuncioViewModel)
        {
            return Mapper.Map<Anuncio, AnuncioViewModel>(_anuncioService.Update(Mapper.Map<AnuncioViewModel, Anuncio>(anuncioViewModel)));
        }

        public void Dispose()
        {
            _anuncioService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
