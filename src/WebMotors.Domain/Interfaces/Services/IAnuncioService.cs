﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotors.Domain.Entitities;
using WebMotors.Infra.WebMotorsApi.Entities;

namespace WebMotors.Domain.Interfaces.Services
{
    public interface IAnuncioService : IServiceBase<Anuncio>
    {
        Task<IEnumerable<Make>> GetMakes();
        Task<IEnumerable<Model>> GetModelsByMake(int makeId);
        Task<IEnumerable<Version>> GetVersionsByModel(int modelId);
        Task<IEnumerable<Vehicle>> GetVehiclesByModel(int modelId);
    }
}
