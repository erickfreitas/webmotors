﻿using WebMotors.Domain.Entitities;

namespace WebMotors.Domain.Interfaces.Repositories
{
    public interface IAnuncioRepository : IRepositoryBase<Anuncio>
    {

    }
}
