﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotors.Domain.Entitities;
using WebMotors.Domain.Interfaces.Repositories;
using WebMotors.Domain.Interfaces.Services;
using WebMotors.Infra.WebMotorsApi.Entities;
using WebMotors.Infra.WebMotorsApi.Interfaces;

namespace WebMotors.Domain.Services
{
    public class AnuncioService : ServiceBase<Anuncio>, IAnuncioService
    {
        private readonly IAnuncioRepository _anuncioRepository;
        private readonly IWebMotorsApiService _webMotorsApiService;

        public AnuncioService(IAnuncioRepository anuncioRepository,
                              IWebMotorsApiService webMotorsApiService) : base(anuncioRepository)
        {
            _anuncioRepository = anuncioRepository;
            _webMotorsApiService = webMotorsApiService;
        }

        public Task<IEnumerable<Make>> GetMakes()
        {
            return _webMotorsApiService.GetMakes();
        }

        public Task<IEnumerable<Model>> GetModelsByMake(int makeId)
        {
            return _webMotorsApiService.GetModelsByMake(makeId);
        }

        public Task<IEnumerable<Vehicle>> GetVehiclesByModel(int modelId)
        {
            return _webMotorsApiService.GetVehiclesByModel(modelId);
        }

        public Task<IEnumerable<Version>> GetVersionsByModel(int modelId)
        {
            return _webMotorsApiService.GetVersionsByModel(modelId);
        }
    }
}
