﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WebMotors.Domain.Entitities;
using WebMotors.Infra.Data.EntityConfig;

namespace WebMotors.Infra.Data.Context
{
    public class WebMotorsContext : DbContext
    {
        public WebMotorsContext() : base("WebMotorsConnection")
        {
            
        }

        public Anuncio AnunciosWebMotors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new AnuncioConfig());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                var erro = e.Message;
                throw new Exception();
            }
        }
    }
}
