﻿using System.Data.Entity.ModelConfiguration;
using WebMotors.Domain.Entitities;

namespace WebMotors.Infra.Data.EntityConfig
{
    public class AnuncioConfig : EntityTypeConfiguration<Anuncio>
    {
        public AnuncioConfig()
        {
            //Table
            ToTable("tb_AnuncioWebmotors");

            //Primary Key
            HasKey(a => a.Id);

            //Properties
            Property(a => a.Id)
                .IsRequired()
                .HasColumnName("ID")
                .HasColumnType("int");

            Property(a => a.Marca)
                .IsRequired()
                .HasColumnName("marca")
                .HasColumnType("varchar")
                .HasMaxLength(45);

            Property(a => a.Modelo)
                .IsRequired()
                .HasColumnName("modelo")
                .HasColumnType("varchar")
                .HasMaxLength(45);

            Property(a => a.Versao)
                .IsRequired()
                .HasColumnName("versao")
                .HasColumnType("varchar")
                .HasMaxLength(45);

            Property(a => a.Ano)
                .IsRequired()
                .HasColumnName("ano")
                .HasColumnType("int");

            Property(a => a.Quilometragem)
                .IsRequired()
                .HasColumnName("quilometragem")
                .HasColumnType("int");

            Property(a => a.Observacao)
                .IsRequired()
                .HasColumnName("observacao")
                .HasColumnType("text");
        }
    }
}
