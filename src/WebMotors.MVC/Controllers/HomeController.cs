﻿using System.Threading.Tasks;
using System.Web.Mvc;
using WebMotors.Application.Interfaces;
using WebMotors.Application.ViewModels;
using WebMotors.Infra.WebMotorsApi.Services;

namespace WebMotors.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAnuncioAppService _anuncioAppService;

        public HomeController(IAnuncioAppService anuncioAppService)
        {
            _anuncioAppService = anuncioAppService;
        }

        public async Task<ActionResult> Index()
        {
            var teste = new WebMotorsApiService();
            var teste11 = await _anuncioAppService.GetMakes();
            var teste1 = await _anuncioAppService.GetModelsByMake(1);
            var teste2 = await _anuncioAppService.GetVehiclesByModel(1);
            var teste3 = await _anuncioAppService.GetVersionsByModel(1);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}