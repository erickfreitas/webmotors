﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebMotors.Application.Interfaces;
using WebMotors.Application.ViewModels;

namespace WebMotors.MVC.Controllers
{
    public class AnunciosController : Controller
    {
        private readonly IAnuncioAppService _anuncioAppService;

        public AnunciosController(IAnuncioAppService anuncioAppService)
        {
            _anuncioAppService = anuncioAppService;
        }

        public ActionResult Index()
        {
            return View(_anuncioAppService.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnuncioViewModel anuncioViewModel = _anuncioAppService.GetById(id.Value);
            if (anuncioViewModel == null)
            {
                return HttpNotFound();
            }
            return View(anuncioViewModel);
        }

        public async Task<ActionResult> Create()
        {
            var makes = await _anuncioAppService.GetMakes();
            ViewBag.Makes = new SelectList(makes, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AnuncioViewModel anuncioViewModel)
        {
            if (ModelState.IsValid)
            {
                var makes = await _anuncioAppService.GetMakes();
                var models = await _anuncioAppService.GetModelsByMake(Convert.ToInt32(anuncioViewModel.Marca));
                var versions = await _anuncioAppService.GetVersionsByModel(Convert.ToInt32(anuncioViewModel.Marca));
                anuncioViewModel.Marca = makes.FirstOrDefault(m => m.Id == Convert.ToInt32(anuncioViewModel.Marca)).Name;
                anuncioViewModel.Modelo = models.FirstOrDefault(m => m.Id == Convert.ToInt32(anuncioViewModel.Modelo)).Name;
                anuncioViewModel.Versao = versions.FirstOrDefault(v => v.Id == Convert.ToInt32(anuncioViewModel.Versao)).Name;
                _anuncioAppService.Add(anuncioViewModel);
                return RedirectToAction("Index");
            }

            return View(anuncioViewModel);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var anuncioViewModel = _anuncioAppService.GetById(id.Value);
            if (anuncioViewModel == null)
            {
                return HttpNotFound();
            }
            var makes = await _anuncioAppService.GetMakes();
            ViewBag.Makes = new SelectList(makes, "Id", "Name");
            return View(anuncioViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AnuncioViewModel anuncioViewModel)
        {
            if (ModelState.IsValid)
            {
                var makes = await _anuncioAppService.GetMakes();
                var models = await _anuncioAppService.GetModelsByMake(Convert.ToInt32(anuncioViewModel.Marca));
                var versions = await _anuncioAppService.GetVersionsByModel(Convert.ToInt32(anuncioViewModel.Marca));
                anuncioViewModel.Marca = makes.FirstOrDefault(m => m.Id == Convert.ToInt32(anuncioViewModel.Marca)).Name;
                anuncioViewModel.Modelo = models.FirstOrDefault(m => m.Id == Convert.ToInt32(anuncioViewModel.Modelo)).Name;
                anuncioViewModel.Versao = versions.FirstOrDefault(v => v.Id == Convert.ToInt32(anuncioViewModel.Versao)).Name;
                _anuncioAppService.Update(anuncioViewModel);
                return RedirectToAction("Index");
            }
            return View(anuncioViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var anuncioViewModel = _anuncioAppService.GetById(id.Value);
            if (anuncioViewModel == null)
            {
                return HttpNotFound();
            }
            return View(anuncioViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var anuncioViewModel = _anuncioAppService.GetById(id);
            _anuncioAppService.Remove(anuncioViewModel);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> GetModels(int id)
        {
            var models = await _anuncioAppService.GetModelsByMake(id);
            return Json(models, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> GetVersions(int id)
        {
            var models = await _anuncioAppService.GetVersionsByModel(id);
            return Json(models, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _anuncioAppService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
