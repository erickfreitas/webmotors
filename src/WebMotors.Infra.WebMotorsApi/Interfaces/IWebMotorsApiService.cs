﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using WebMotors.Infra.WebMotorsApi.Entities;

namespace WebMotors.Infra.WebMotorsApi.Interfaces
{
    public interface IWebMotorsApiService
    {
        HttpClient CreateHttpClient(string urlComplement);
        Task<IEnumerable<Make>> GetMakes();
        Task<IEnumerable<Model>> GetModelsByMake(int makeId);
        Task<IEnumerable<Version>> GetVersionsByModel(int modelId);
        Task<IEnumerable<Vehicle>> GetVehiclesByModel(int modelId);
    }
}
