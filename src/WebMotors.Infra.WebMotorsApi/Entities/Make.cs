﻿namespace WebMotors.Infra.WebMotorsApi.Entities
{
    public class Make
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
