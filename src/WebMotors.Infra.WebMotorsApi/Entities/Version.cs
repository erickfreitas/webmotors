﻿namespace WebMotors.Infra.WebMotorsApi.Entities
{
    public class Version
    {
        public int ModelId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
