﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using WebMotors.Infra.WebMotorsApi.Entities;
using WebMotors.Infra.WebMotorsApi.Interfaces;

namespace WebMotors.Infra.WebMotorsApi.Services
{
    public class WebMotorsApiService : IWebMotorsApiService
    {
        protected readonly string _url = ConfigurationManager.AppSettings["ApiUrl"];

        public HttpClient CreateHttpClient(string urlComplement)
        {
            var client = new HttpClient { BaseAddress = new Uri(_url + urlComplement) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public async Task<IEnumerable<Make>> GetMakes()
        {
            var client = CreateHttpClient("/Make");
            var response = client.GetAsync($"{_url}/Make").Result;
            if (!response.IsSuccessStatusCode) return null;
            var responseStringJson = await response.Content.ReadAsStringAsync();
            return new JavaScriptSerializer().Deserialize<IEnumerable<Make>>(responseStringJson);
        }

        public async Task<IEnumerable<Model>> GetModelsByMake(int makeId)
        {
            var client = CreateHttpClient($"/Model?MakeID={makeId}");
            var response = client.GetAsync($"{_url}/Model?MakeID={makeId}").Result;
            if (!response.IsSuccessStatusCode) return null;
            var responseStringJson = await response.Content.ReadAsStringAsync();
            return new JavaScriptSerializer().Deserialize<IEnumerable<Model>>(responseStringJson);
        }

        public async Task<IEnumerable<Vehicle>> GetVehiclesByModel(int modelId)
        {
            var client = CreateHttpClient($"/Vehicles?Page={modelId}");
            var response = client.GetAsync($"{_url}/Vehicles?Page={modelId}").Result;
            if (!response.IsSuccessStatusCode) return null;
            var responseStringJson = await response.Content.ReadAsStringAsync();
            return new JavaScriptSerializer().Deserialize<IEnumerable<Vehicle>>(responseStringJson);
        }

        public async Task<IEnumerable<Entities.Version>> GetVersionsByModel(int modelId)
        {
            var client = CreateHttpClient($"/Version?ModelID={modelId}");
            var response = client.GetAsync($"{_url}/Version?ModelID={modelId}").Result;
            if (!response.IsSuccessStatusCode) return null;
            var responseStringJson = await response.Content.ReadAsStringAsync();
            return new JavaScriptSerializer().Deserialize<IEnumerable<Entities.Version>>(responseStringJson);
        }
    }
}
